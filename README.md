# TUmux off-gas multiplexer
Hardware designs for the TUmux off-gas multiplexer.

## Hardware versions
These designs are based around National Instruments compactRIO controllers.

## License

This project is licensed under the [CERN Open Hardware Licence Version 2 - Strongly Reciprocal](LICENSE.md).

## Contact

For any questions or inquiries, please contact Dirk Geerts at d.j.m.geerts@tudelft.nl.
